//
//  main.m
//  iOSDB-To-FMDB
//
//  Created by IPA-Co on 6/17/16.
//  Copyright © 2016 IPA-Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
