//
//  ViewController.m
//  iOSDB-To-FMDB
//
//  Created by IPA-Co on 6/17/16.
//  Copyright © 2016 IPA-Co. All rights reserved.
//

#import "ViewController.h"
#import "iOSDB.h"

@interface ViewController ()
{
    iOSDB *dbHelper;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        // init
        dbHelper=[iOSDB setupWithFileName:@"test" extension:@"db" version:@""];
        BOOL isClearSuccess=[dbHelper clearTable:@"tblTest"];
        if(!isClearSuccess)
            return ;
        
        // insert
        NSLog(@"\n>> Insert first row << \n");
        BOOL isInsetSuccess=[dbHelper insertToTable:@"tblTest" elements:@{@"ID": @"1111", @"name": @"navid"}];
        if(isInsetSuccess)
            [self logTable];
        
        [NSThread sleepForTimeInterval:5];
        // update
        NSLog(@"\n>> Update row with ID 1111 <<\n");
        BOOL isUpdateSuccess=[dbHelper updateTable:@"tblTest" withControlKey:@{@"ID": @"1111"} andElements:@{@"name": @"NAVID"}];
        if(isUpdateSuccess)
            [self logTable];
        
        [NSThread sleepForTimeInterval:5];
        // insert
        NSLog(@"\n>> Insert Second row <<\n");
        isInsetSuccess=[dbHelper insertToTable:@"tblTest" elements:@{@"ID": @"2222", @"name": @"bob"}];
        if(isInsetSuccess)
            [self logTable];
        
        [NSThread sleepForTimeInterval:5];
        // delete
        NSLog(@"\n\n\n\n");
        NSLog(@"\n>> Delete row with ID 1111 <<\n");
        BOOL isDeleteSuccess=[dbHelper deleteFromTable:@"tblTest" withControlKey:@"ID" andValue:@"1111" andIsNumeric:YES];
        if(isDeleteSuccess)
            [self logTable];
    });
   
    
}

-(void) logTable{
    
    NSArray *arResult=[dbHelper selectWithQuery:@"SELECT * FROM tblTest"];
    for (int i=0; i<arResult.count; i++) {
        NSLog(@"Row:%d ID:%@ name:%@\n", i, [arResult[i] valueForKey:@"ID"], [arResult[i] valueForKey:@"name"]);
    }
    NSLog(@"\n\n\n\n");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
